#include <algorithm>
#include <fstream>
#include <iterator>
#include <iostream>
#include <string>
#include <string_view>
#include <vector>

namespace {

bool funnel(std::string_view haystack, std::string_view needle) {
  if (haystack.size() != needle.size() + 1) {
    return false;
  }
  auto [h,n] = std::mismatch(
      haystack.begin(), haystack.end(), needle.begin(), needle.end());
  return std::equal(h + 1, haystack.end(), n);
}

using WordList = std::vector<std::string>;
auto readWords(const char* filename) {
  using It = std::istream_iterator<std::string>;
  auto in = std::ifstream(filename);
  return WordList(It(in), It());
}

auto shorten(const WordList& dict, std::string_view word) {
  std::vector<std::string> result(word.size(), std::string(word));
  auto idx = 0;
  for (auto& w : result) {
    w.erase(idx++, 1);
  }
  auto it = std::remove_if(
      result.begin(),
      result.end(),
      [&dict](const auto& w) {
        return !std::binary_search(dict.begin(), dict.end(), w);
      });
  it = std::unique(result.begin(), it);
  result.erase(it, result.end());
  return result;
}

}

int main(int, char** argv) {
  std::ios::sync_with_stdio(false);
  auto dict = readWords(argv[1]);
  for (const auto& w : dict) {
    if (shorten(dict, w).size() == 5) {
      std::cout << w << '\n';
    }
  }
}
