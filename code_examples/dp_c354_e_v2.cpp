#include <iostream>

int smallestFctrSum(int num)
{
    int tempFctr = 1;
    int count = 1;
    while (true)
    {
        if ((num % count) == 0)
        {
          if ((num / count) == tempFctr)
                return count + tempFctr;
            else
                tempFctr = count;
        }
        count++;
    }
}

int main()
{
    int num = 345678 ;

    std::cout << num << " => " << smallestFctrSum(num) << '\n';

    return 0;
}
