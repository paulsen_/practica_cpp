// classes example
#include <iostream>
#include "../include/util.h"


//using namespace std;

/* Clase:
Usada para especificar tipos personalizados de datos (como si se tratara de un int, float, double, string)
Sus miembros por defecto son privados (solo pueden ser accesados por miembros de la Clase)
Aquellos publicos son accesibles por fuera de la clase (una mezcla entre un subprograma y un tipo de dato)

En este caso Rectangulo tiene dos datos privados, ancho y alto; sus miembros publicos (accesibles por fuera de la clase) incluyen un procedimiento
llamado iniciar_valores, que toma dos argumentos en int.

Tambien incluye una funcion llamada area, que retorna el calculo entre el ancho y el alto */

class Rectangulo {
  private:
    double ancho, alto;
  public:
    void iniciar_valores (std::string, std::string);
    double area() {return ancho * alto;}
    double perimetro() {return ((ancho * 2)+ (alto * 2));}
};

/* Los procedimientos que necesitan realizar acciones por fuera de la clase se deben llamar por su espacio de trabajo (el nombre de la clase)
Seguido por el nombre de la funcion especificada por la misma, debido a que este procedimiento es parte de la clase,
puede modificar los valores de alto y ancho.

La funcion de abajo sirve de intermediaria para poder modificar los valores privados de la clase */


void Rectangulo::iniciar_valores (std::string x, std::string y) {
  ancho = convcp(x);
  alto = convcp(y);
}


/* Tal como si inicializaramos un int, o un string, se inicializa una variable rect, de tipo Rectangulo,
luego llamamos al procedimiento intermediario que fija los valores, y luego llamamos a la funcion que hace el procesamiento de aquellos valores. */

int main () {
  Rectangulo rect;
  std::string temp1, temp2;
  std::cout << "\nIngrese el ancho del Rectangulo: ";
  std::cin >> temp1;
  std::cout << "\nIngrese el alto del Rectangulo: ";
  std::cin >> temp2;
  rect.iniciar_valores (temp1, temp2);
  std::cout << "\nArea del rectangulo: " << rect.area() << "\n";
  std::cout << "\nPerimetro del rectangulo: " << rect.perimetro() << '\n';
  return 0;
}
