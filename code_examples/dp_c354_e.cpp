#include <iostream>
#include <cmath>



long long smallestFctrSum(long long num)
{
    long long tempFctr{ 1 };
    long long count{ 1 };

    while (true)
    {
        if ((num % count) == 0)
        {
            if ((num / count) == tempFctr)
                return count + tempFctr;
            else
                tempFctr = count;
        }

        count++;
    }
}

int main(int argc, char const *argv[]) {
  long long int A, B, solution = A + 1, sum = 0;
  std::cout << "Input number A:" << '\n';
  std::cin >> A;
  for(B = 1; B <= (long long int)sqrt(A); ++B){
        if(A % B == 0){
            sum = B + (A/B);
            if(sum < solution) solution = sum;
        }
    }
  std::cout << "Solution: " << solution << '\n';
}
