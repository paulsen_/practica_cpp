#include <iostream>
#include <vector>
#include <algorithm> /* para la funcion std::replace */
#include <random> /* para las funciones srand y rand() */
#include <ctime> /* para la funcion time proveniente del lenguaje C */
#include <stdlib.h> /* para hacer llamadas de sistema especificas a windows (system("cls")) */
#include <conio.h> /* para hacer llamadas especificas a la consola de Windows (getch()) */
#include "include/simplerandom.h"

using namespace std;

const double version = 0.1;
int tVec, modo, eleccion;

/* Generacion de numeros aleatorios */

int main(int argc, char *argv[]) {
	string operacion;
	/*double modulovector, sumamodvector;*/

	/* Crear un vector en donde pueda guardar el alfabeto en de ASCII */
	std::vector<char> alfabeto(26);
	std::iota(alfabeto.begin(), alfabeto.end(), 'a');

	cout << "Ingrese [1] para el Modo manual.\n";
	cout << "Ingrese [2] para el Modo automatico.\n\n\n\n";
	cout << "Seleccione el modo para utilizar: ";
	cin >> modo;

	cout << "Ingrese [1] para sumar dos vectores.\n";
	cout << "Ingrese [2] para restar dos vectores.\n\n\n\n";
	cout << "Seleccione la operacion a realizar: ";
	cin >> eleccion;

	if (modo == 2) {
		cout << "Modo automatico -- Ingresando valores automaticamente.";
		cout << "\nPresione cualquier tecla para continuar...";
		getch();
	}

	system("cls");
	if (eleccion == 1) {
		operacion = "suma";
		cout << "Suma de Vectores.\n\n";
	}
	if (eleccion == 2) {
		operacion = "resta";
		cout << "Resta de Vectores.\n\n";
	}

	if (modo == 1) {
		cout << "Ingrese la cantidad de componentes de los vectores: ";
		cin >> tVec;
	}
	else if (modo == 2) {
		tVec = randomINT(1, 5);
	}

	vector<double> vector1(tVec);
	vector<double> vector2(tVec);
	vector1.reserve(tVec);
	vector2.reserve(tVec);
	string temp;

	for (int i = 0; i < tVec; i++) {
		system("cls");
		if (modo == 1) {
			cout << "Ingrese el valor de la componente " << alfabeto[i] << " del vector 1: ";
			cin >> temp;
			std::string::size_type sz;
			/* Replace, parte del modulo std::algorithm, evalua la variable temp desde el principio hasta el final, y remplaza las comas por puntos */
			std::replace(temp.begin(), temp.end(), ',', '.');
			/* Luego el temp es convertida usando stod (string to double) y inmediatamente asignada al indice determinado por el valor
			   actual de i del vector alumnos */
			vector1.at(i) = stod(temp, &sz);
		}
		if (modo == 2) {
			vector1.at(i) = (randomDP(1.00, 20.00));
		}
	}
	for (int i = 0; i < tVec; i++) {
		system("cls");
		if (modo == 1) {
			cout << "Ingrese el valor de la componente " << alfabeto[i] << " del vector 2: ";
			cin >> temp;
			std::string::size_type sz;
			/* Replace, parte del modulo std::algorithm, evalua la variable temp desde el principio hasta el final, y remplaza las comas por puntos */
			std::replace(temp.begin(), temp.end(), ',', '.');
			/* Luego el temp es convertida usando stod (string to double) y inmediatamente asignada al indice determinado por el valor
			 actual de i del vector alumnos */
			vector2.at(i) = stod(temp, &sz);
		}
		if (modo == 2) {
			vector2.at(i) = (randomDP(1.00, 20.00));
		}
	}

	vector<double> vectorresultado(tVec);
	vectorresultado.reserve(tVec);

	if (operacion == "suma") {
		for (int i = 0; i < tVec; i++) {
			vectorresultado.at(i) = (vector1[i] + vector2[i]);
			cout << "El valor resultante de la componente " << alfabeto[i] << " del vector resultante es: " << vectorresultado[i] << "\n";
		}
	}
	if (operacion == "resta") {
		for (int i = 0; i < tVec; i++) {
			vectorresultado.at(i) = (vector1[i] - vector2[i]);
			cout << "El valor resultante de la componente " << alfabeto[i] << " del vector resultante es: " << vectorresultado[i] << "\n";
		}
	}
	cout << "\nPresione cualquier tecla para continuar...";
	getch();

	/* for (int i = 0; i < vectorresultado.size(); i++) {
	  sumamodvector = (sumamodvector + static_cast<double>(vectorresultado.at(i) * 2));
	}
	modulovector = sqrt(sumamodvector);

	cout << "\n\nEl valor del modulo del vector resultante es " << modulovector << ".\n\n";
	cout << "\nPresione cualquier tecla para continuar...";
	getch(); */

	return 0;
}
