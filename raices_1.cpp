#include <iostream>
#include <cmath>
using namespace std;

int main(int argc, char *argv[]) {
  float a;
  float b;
  float c;
  cout <<  "\nIngrese el coeficiente A: ";
  cin >> a;
  cout <<  "\nIngrese el coeficiente B: ";
  cin >> b;
  cout <<  "\nIngrese el coeficiente C: ";
  cin >> c;
  float det = b * b - 4 * a * c;
  if (det < 0) {
      float p_real = -b / (2 * a);
      float p_imag = sqrt(-det) / (2 * a);
      cout << "Las raices son: "
          << p_real << "+" << p_imag << "i y "
          << p_real << "-" << p_imag << "i" << endl;
    } else {
      float raiz1 = (-b + sqrt(det)) / (2 * a);
      float raiz2 = (-b - sqrt(det)) / (2 * a);
      cout << "Las raices son: " << raiz1 << " y "
          << raiz2 << endl;
    }
    return 0;
}
