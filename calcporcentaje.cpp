#include <iostream>

using namespace std;

int varones, mujeres;
double porc_var, porc_muj;

void porcentaje(int param1, int param2) {
  int total;
  total = param1 + param2;
  porc_var = (100 * param1) / total;
  porc_muj = (100 * param2) / total;

}

int main() {
  cout << "Ingrese la cantidad de varones: ";
  cin >> varones;
  cout << "Ingrese la cantidad de mujeres: ";
  cin >> mujeres;
  porcentaje(varones, mujeres);
  cout << "El porcentaje de varones es: " << porc_var << "%\n";
  cout << "El porcentaje de mujeres es: " << porc_muj << "%\n";
  return 0;
}
