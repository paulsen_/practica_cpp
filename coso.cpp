#include <iostream>
#include <ctime>
#include <stdlib.h>
#include <conio.h>
#include <cstring> /* para la funcion strcmp proveniente del lenguaje C */
#include "include/simplerandom.h"

using namespace std;

int pelotas, latas, cant_personas, persona, latas_persona, cant_premio, autoprueba;

int main(int argc, char* argv[]) {
  if (argc > 1) {
    for (int i = 0; i < argc; ++i) {
      /* Autoprueba es un argumento evaluado para activar la variable autoprueba = 1, con lo que el programa genera los valores de cada variable
      que normalmente el usuario deberia ingresar*/
      if (strcmp(argv[i], "--autoprueba") == 0) {
        autoprueba = 1;
      }
    }
  }
  system("cls");
  if (autoprueba == 1) {
  cout << "\n\nDEBUG: PRUEBA AUTOMATICA DEFINIDA.\n\n";
  cout << "\nPresione cualquier tecla para continuar...";
  getch();
  system("cls");
  }

  if (autoprueba == 1) {
  cout << "\n\nDEBUG: PRUEBA AUTOMATICA DEFINIDA.\n\n";
  }
  cout << "Ingrese la cantidad de personas: ";
  if (autoprueba == 1) {
  cant_personas = randomINT(1,15);
  cout << "\nDefinido automaticamente: " << cant_personas;
  } else {
  cin >> cant_personas;
  }
  for (persona = 1; persona <= cant_personas;persona++) {
      cout << "\nIngrese la cantidad de latas que tiro la persona " << persona << ": ";
      if (autoprueba == 1) {
      latas_persona = randomINT(0,12);
      cout << "\nDefinido automaticamente: " << latas_persona;
      } else {
      cin >> latas_persona;
      }
      latas = latas + latas_persona;
      if (latas_persona >= 12) {
        cant_premio++;
      }
    }
  cout << "\nLa cantidad de premios es: " << cant_premio << "\n\n";;
  cout << "\nLa cantidad de personas en el dia fue: " << cant_personas << "\n\n";

  if (autoprueba == 1) {
  cout << "\n\nDEBUG: PRUEBA AUTOMATICA TERMINADA.\n\n";
  cout << "\nPresione cualquier tecla para continuar...\n";
  getch();
  }
  return 0;


}
