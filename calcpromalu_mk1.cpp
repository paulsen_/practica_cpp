#include <iostream>
#include <vector> /* para la funcion std::vector */
#include <array>
#include <string>
#include <stdlib.h> /* para hacer llamadas de sistema especificas a windows (system("cls")) */
#include <conio.h> /* para hacer llamadas especificas a la consola de Windows (getch()) */
#include <cctype>
#include <fstream>
#include "include/simplerandom.h"
#include "include/util.h"


/* using namespace std;
No usar namespace std, porque no tiene sentido. */


// Definir las variables a usar

const double version = 2.00;
int cantAlu, eleccion, autoprueba = 0, cantMay = 0, debug = 0;
std::string temp;
double mediaDP, suma = 0.00;



void fautoprueba() {
  autoprueba = 1;
  cantAlu = (randomINT(0, 20));
  system("cls");
  std::cout << "AUTOPRUEBA: Ingresando valores de prueba automaticamente\n\n";
  std::cout << "Presione cualquier tecla para continuar...";
  getch();
}

int main(int argc, char* argv[]) {

/* En el caso de que existan argumentos de ejecucion, se ejecutan una de multiples acciones */


if (autoprueba != 1) {
  system("cls");
  std::cout << "OPCIONES: \n\n\n";
  std::cout << "Ingrese 1 para modo manual.\n";
  std::cout << "Ingrese 2 para prueba automatica.\n\n";
  std::cout << "Ingrese su eleccion: ";

  std::cin >> eleccion; // termina cuando se introducen espacios o nuevas lineas
  system("cls");
  switch(eleccion) {
  case 1:
          autoprueba = 0;
          break;
  case 2:
          fautoprueba();
          break;
  default:
          system("cls");
          std::cout << "Opcion Erronea\n";
  }
}



/* Si la variable de inicio --autoprueba no ha sido definida,
   El usuario ingresara la cantidad de alumnos */
if (autoprueba != 1) {
  system("cls");
  std::cout << "Ingrese la cantidad de alumnos: ";
  std::cin >> cantAlu;
}

/* Crear el vector:
 Los vectores en C++ tienen el formato:

 std::vector<tipo> nombre(tamaño)

 En este caso creamos un vector que almacena datos reales de doble precision,
 y su tamaño depende del valor de la variable entera cantAlu.

 La diferencia entre vectores y arrays es que los vectores se les puede cambiar su tamaño de forma dinamica, a estos efectos se usa
 alumnos.reserve para reservar dinamicamente en la memoria necesaria para almacenar el numero de variables especificadas del vector.

 En el caso de que cambiemos el tamaño del vector, la llamada .reserve se ejecutara nuevamente y movera todo el vector a un nuevo espacio de memoria
 Donde el vector entre con su nuevo tamaño.
 */


std::vector<int> alumnosleg(cantAlu);
//std::iota(alumnosnum.begin(), alumnosnum.end(), '1');
std::vector<std::string> alumnosnombre(cantAlu);
std::vector<std::string> alumnosapellido(cantAlu);
std::vector<std::string> randnombre(20);
std::vector<std::string> randapellido(20);
std::vector<double> alumnosprom(cantAlu);
alumnosleg.reserve(cantAlu);
alumnosnombre.reserve(cantAlu);
alumnosapellido.reserve(cantAlu);
alumnosprom.reserve(cantAlu);
randnombre.reserve(cantAlu);
randapellido.reserve(cantAlu);
std::string nomtemp, aplltemp, promtemp, coso1, coso2, term;


/* En este loop, se ejecutan ciertas acciones una cierta cantidad de veces, hasta que la variable que creamos especificamente para esto
   Alcance un valor deaseado, en este caso, se ejecutaran las acciones hasta que i alcance el valor de cantAlu, y por cada ejecucion se le añadira 1 al valor de i

   Terminada la ejecucion, el valor de la variable i es borrado, y la variable i, destruida (borrada de la memoria)
                                                                                                                                                                    */
for (int i = 0; i < cantAlu; i++) {
  if (autoprueba == 1) {
    int randomIndex;
    std::ifstream nombres("extra/nombres.txt");
    while (getline(nombres, coso1))
    {
        randnombre.push_back(coso1);
    }
    std::ifstream apellidos("extra/apellidos.txt");
    while (getline(apellidos, coso2))
    {
        randapellido.push_back(coso2);
    }
    alumnosprom.at(i) = randomDP(0, 10.00);
    alumnosleg.at(i) = randomINT(40000, 49999);
    randomIndex = rand() % randnombre.size();
    alumnosnombre[i] = randnombre[randomIndex];
    randomIndex = rand() % randapellido.size();
    alumnosapellido[i] = randapellido[randomIndex];
  } else {
    /* Si la variable de inicio --autoprueba no ha sido definida,
       El usuario ingresara la altura del alumno, por cada uno de los alumnos
       La funcion system hace una llamada de sistema al comando de Windows "cls", que borra todo el contenido de la pantalla */
    system("cls");
    std::cout << "La cantidad de alumnos es: " << cantAlu << "\n\n";
    std::cout << "\nIngrese el nombre del alumno " << (i + 1) << "\n";
    /* Manejar la posibilidad de que el usuario ingrese un numero con coma en lugar de un punto
       Por esto la variable temp es una cadena */
    std::cin >> nomtemp;
    std::cout << "\nIngrese el apellido del alumno " << (i + 1) << "\n";
    /* Manejar la posibilidad de que el usuario ingrese un numero con coma en lugar de un punto
       Por esto la variable temp es una cadena */
    std::cin >> aplltemp;
    /* Luego el temp es convertida usando stod (string to double) y inmediatamente asignada al indice determinado por el valor
         actual de i del vector alumnos */
    alumnosapellido.at(i) = aplltemp;
    std::cout << "\nIngrese el promedio del alumno " << (i + 1) << "\n";
    /* Manejar la posibilidad de que el usuario ingrese un numero con coma en lugar de un punto
       Por esto la variable temp es una cadena */
    std::cin >> promtemp;
    /* Luego el temp es convertida usando stod (string to double) y inmediatamente asignada al indice determinado por el valor
         actual de i del vector alumnos */
    alumnosprom.at(i) = convcpSTDP(promtemp);

  }
}

/* Sumar los valores de cada indice dentro del vector alumnos
   para obtener la altura combinada de todos los alumnos.*/
for (int i = 0; i < cantAlu; i++) {
    suma = suma + alumnosprom.at(i);
    }

/* mediaDP es el valor de a altura media de todos los alumnos en doble precision:
 Se obtiene con la suma de todas las alturas, dividida por la cantidad de alumnos
 En este caso la cantidad de alumnos se transforma en el momento de un entero (int)
 a un real de doble precision (double) para que la division sea entre datos del mismo tipo */
mediaDP = (suma / static_cast<double>(cantAlu));

/* Obtener la cantidad de alumnos que superan la altura media ,
   Por cada alumno dentro del vector alumnos, se compara su altura con la altura media,
   Si la altura del alumno es mayor a la media, se le suma 1 a la variable cantMay */


std::ofstream pepe;
pepe.open("nombap.txt");
for (int i = 0; i < cantAlu; i++) {
  if (alumnosprom[i] > mediaDP) {
    cantMay = cantMay + 1;
	pepe << alumnosnombre[i] << ", " << alumnosapellido[i] << "\n";
    }
}
pepe.close();

  /* En el caso de que se haya usado la variable de inicio --debug, mostrar el valor de cada indice de
     cada alumno en el vector alumnos, y valor de las variables que son resultado directo de los valores guardados
     dentro del vector alumnos */
  /*if (debug == 1) {
  std::cout << "alumnos[" << i << "] : " << alumnos[i]  << "\n";
  std::cout << "MediaDP:" << mediaDP << "\n";
  std::cout << "cantMay: " << cantMay << "\n";
  }
}*/
  /*if (debug != 1) {
    system("cls");
  }

  /* Finalmente convertir la variable mediaDP a un string y reemplazar el . por una , por propositos de localizacion.
     Luego mostrar los valores calculados y salir del programa.
  */


std::cout << "La cantidad de alumnos es: " << cantAlu << "\n";
std::cout << "El promedio entre todos los alumnos es : " << convcpDPST(mediaDP) << "\n";
std::cout << "La cantidad de alumnos que superan el promedio es: " << cantMay << "\n\n";
std::cout << "Los alumnos que superan el promedio son: \n";

std::ifstream finstring("nombap.txt");
for (int i = 0; i < cantAlu; i++) {
  while (getline(finstring, term))
  {
      std::cout << term << '\n';
  }
}
finstring.close();
std::cout << "\nPresione cualquier tecla para continuar...\n\n";

getch();
return 0;
}
