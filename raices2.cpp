#include <iostream>
#include <cmath>

using namespace std;

int main(int argc, char *argv[]) {
    float a,b,c;
    cout << "Coeficientes A B C: ";
    cin >> a >> b >> c;
    float det = b * b - 4 * a * c;
    if (det < 0) {
      float part_real = -b / (2 * a);
      float part_imag = sqrt(-det) / (2 * a);
      cout << "Las raices son : "
           << part_real << "+" << part_imag << "i y "
           << part_real << "-" << part_imag << "i" << endl;
      } else {
        float raiz1 = (-b+sqrt(det))/(2*a);
        float raiz2 = (-b-sqrt(det))/(2*a);
        cout << "Las raices son: " << raiz1 << " y "
          << raiz2 << endl;
      }
    return 0;
}
