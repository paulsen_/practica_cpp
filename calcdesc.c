#include <stdio.h>

double porc_var, porc_muj;

void porcentaje(int varones1, int mujeres1) {
  int total;

  total = varones1 + mujeres1;
  porc_var = (100*varones1)/total;
  porc_muj = (100*mujeres1)/total;
}


int main()
{
  int varones, mujeres;

   printf( "Introduzca la cantidad de varones: " );
   scanf( "%i", &varones );
   printf( "Introduzca la cantidad de mujeres: " );
   scanf( "%i", &mujeres );
   porcentaje(varones, mujeres);
   printf("El porcentaje de varones es %d\nEl porcentaje de mujeres es %d\n" % (porc_var, porc_muj);
   return 0;
}
