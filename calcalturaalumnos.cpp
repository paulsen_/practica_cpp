#include <iostream>
#include <algorithm> /* para la funcion std::replace */
#include <vector> /* para la funcion std::vector */
#include <cstring> /* para la funcion strcmp proveniente del lenguaje C */
#include <stdlib.h> /* para hacer llamadas de sistema especificas a windows (system("cls")) */
#include <conio.h> /* para hacer llamadas especificas a la consola de Windows (getch()) */
#include "include/simplerandom.h"


using namespace std;

// Definir las variables a usar

const int version = 10092018;
////////////////////////////////////////////////////////////////////////////////////////
/* La version de la exposicion? 10/09/2018? */
////////////////////////////////////////////////////////////////////////////////////////

int cantAlu, eleccion, autoprueba, cantMay = 0, debug = 0;

double mediaDP, suma = 0.00;

int main(int argc, char* argv[]) {

/* En el caso de que existan argumentos de ejecucion, se ejecutan una de multiples acciones */

if (argc > 1) {
  for (int i = 0; i < argc; ++i) {
    /* Autoprueba es un argumento evaluado para activar la variable autoprueba = 1, con lo que el programa genera los valores de cada variable
    que normalmente el usuario deberia ingresar*/
    if (strcmp(argv[i], "--autoprueba") == 0) {
      autoprueba = 1;
      cantAlu = 50;
      cout << "AUTOPRUEBA: Ingresando valores de prueba automaticamente\n\n";
      cout << "Presione cualquier tecla para continuar...";

      /* Get Character -- getch(), parte de la libreria conio.h (Console I/O header)
         Normalmente getch() espera a que ingrese un caracter especificado, que deberia estar dentro del parentesis.
         Como no hay nada en el parentesis, getch esta esperando cualquier caracter.
         Como el valor que obtiene getch no es asignado a ninguna funcion ni existe otro condicional que necesite procesamiento, se continua la ejecucion. */
      getch();
    }
    /* Mostrar los valores de las variables que dependen de algun calculo matematico
    y de que sean propiamente definidas antes de la ejecucion de otras tareas;
    cuando se pase el argumento --debug */
    if (strcmp(argv[i], "--debug") == 0) {
      debug = 1;
    }
    /* Mostrar la version del programa al pasar el argumento --version */
    if ((strcmp(argv[i], "--version") == 0) || (strcmp(argv[i], "-v") == 0)) {
      cout << "calcalturaalumnos.cpp version " << version;
    return 0;
    }
  }
}


if (autoprueba != 1) {
  cout << "Calculadura de altura de alumnos " << version << '\n';
  cout << "Para la clase de Algoritmos 1K1 de la UTN FRM\n"
  cout << "OPCIONES: \n\n\n";
  cout << "Ingrese 1 para modo manual.\n";
  cout << "Ingrese 2 para prueba automatica.\n\n";

  cout << "Ingrese su eleccion: ";
  cin >> eleccion;
  if (eleccion == 2) {
      autoprueba = 1;
      system("cls");
      cantAlu = 50;
      cout << "AUTOPRUEBA: Ingresando valores de prueba automaticamente\n\n";
      cout << "Presione cualquier tecla para continuar...";
      getch();
    } else {
      autoprueba = 0;
    }
 }


/* Si la variable de inicio --autoprueba no ha sido definida,
   El usuario ingresara la cantidad de alumnos */
if (autoprueba != 1) {
  system("cls");
  cout << "Ingrese la cantidad de alumnos: ";
  cin >> cantAlu;
}

/* Crear el vector:
 Los vectores en C++ tienen el formato:

 std::vector<tipo> nombre(tamaño)

 En este caso creamos un vector que almacena datos reales de doble precision,
 y su tamaño depende del valor de la variable entera cantAlu.

 La diferencia entre vectores y arrays es que los vectores se les puede cambiar su tamaño de forma dinamica, a estos efectos se usa
 alumnos.reserve para reservar dinamicamente en la memoria necesaria para almacenar el numero de variables especificadas del vector.

 En el caso de que cambiemos el tamaño del vector, la llamada .reserve se ejecutara nuevamente y movera todo el vector a un nuevo espacio de memoria
 Donde el vector entre con su nuevo tamaño.
 */

vector<double> alumnos(cantAlu);
alumnos.reserve(cantAlu);

/* Un string (cadena) para guardar algo temporalmente */
string temp;


/* En este loop, se ejecutan ciertas acciones una cierta cantidad de veces, hasta que la variable que creamos especificamente para esto
   Alcance un valor deaseado, en este caso, se ejecutaran las acciones hasta que i alcance el valor de cantAlu, y por cada ejecucion se le añadira 1 al valor de i

   Terminada la ejecucion, el valor de la variable i es borrado, y la variable i, destruida (borrada de la memoria)
                                                                                                                                                                    */
for (int i = 0; i < cantAlu; i++) {
  if (autoprueba == 1) {
    /* Si la variable de inicio autoprueba ha sido definida, se le asignara un valor aleatorio generado entre 1,40 y 2,00 metros
       que se guardara en cada uno de los indices del vector alumnos
       Aqui se usa el subprograma random, con el formato random(min, max) siendo min y max los rangos a trabajar especificados en doble precision.
    */
    alumnos.at(i) = randomDP(1.40, 2.00);
  } else {
    /* Si la variable de inicio --autoprueba no ha sido definida,
       El usuario ingresara la altura del alumno, por cada uno de los alumnos
       La funcion system hace una llamada de sistema al comando de Windows "cls", que borra todo el contenido de la pantalla */
    system("cls");
    cout << "La cantidad de alumnos es: " << cantAlu << "\n\n";
    cout << "Ingrese la estatura del alumno " << (i + 1) << " en metros: \n";
    /* Manejar la posibilidad de que el usuario ingrese un numero con coma en lugar de un punto
       Por esto la variable temp es una cadena */
    cin >> temp;
    std::string::size_type sz;
    /* Replace, parte del modulo std::algorithm, evalua la variable temp desde el principio hasta el final, y remplaza las comas por puntos */
    std::replace(temp.begin(), temp.end(), ',', '.');
    /* Luego el temp es convertida usando stod (string to double) y inmediatamente asignada al indice determinado por el valor
       actual de i del vector alumnos */
    alumnos.at(i) = stod(temp,&sz);
  }
}

/* Sumar los valores de cada indice dentro del vector alumnos
   para obtener la altura combinada de todos los alumnos.*/
for (int i = 0; i < cantAlu; i++) {
    suma = suma + alumnos.at(i);
    }

/* mediaDP es el valor de a altura media de todos los alumnos en doble precision:
 Se obtiene con la suma de todas las alturas, dividida por la cantidad de alumnos
 En este caso la cantidad de alumnos se transforma en el momento de un entero (int)
 a un real de doble precision (double) para que la division sea entre datos del mismo tipo */
mediaDP = (suma / static_cast<double>(cantAlu));

/* Obtener la cantidad de alumnos que superan la altura media ,
   Por cada alumno dentro del vector alumnos, se compara su altura con la altura media,
   Si la altura del alumno es mayor a la media, se le suma 1 a la variable cantMay */
for (int i = 0; i < cantAlu; i++) {
  if (alumnos[i] > mediaDP) {
    cantMay = cantMay + 1;
  }
  /* En el caso de que se haya usado la variable de inicio --debug, mostrar el valor de cada indice de
     cada alumno en el vector alumnos, y valor de las variables que son resultado directo de los valores guardados
     dentro del vector alumnos */
  if (debug == 1) {
  cout << "alumnos[" << i << "] : " << alumnos[i]  << "\n";
  cout << "MediaDP:" << mediaDP << "\n";
  cout << "cantMay: " << cantMay << "\n";
  }
}
  if (debug != 1) {
    system("cls");
  }

  /* Finalmente convertir la variable mediaDP a un string y reemplazar el . por una , por propositos de localizacion.
     Luego mostrar los valores calculados y salir del programa.
  */

  string mediastring = std::to_string(mediaDP);
  std::replace(mediastring .begin(), mediastring.end(), '.', ',');
  cout << "La cantidad de alumnos es: " << cantAlu << "\n";
  cout << "La estatura media es: " << mediastring << "\n";
  cout << "La cantidad de alumnos con estatura mayor a la media es: " << cantMay << "\n\n";

  cout << "\nPresione cualquier tecla para continuar...";

  getch();
  return 0;

}
