/* Este programa originalmente consistia de la consigna siguiente:

   "Desarrolle un programa que le informe al farmacéutico el porcentaje de descuento a aplicar a los afiliados de una
   Obra Social. Sabiendo que si el afiliado tiene Plan A, todas las recetas que presente tendrán un descuento del 100%.
   En el caso que se trate de un Plan B o C, el afiliado tendrá un descuento del 90%; y si es Plan D un 50%. Para el resto
   de los planes, sólo tendrá el 25%. Si el afiliado paga en efectivo, la farmacia lo beneficia con el descuento del 5%
   además del descuento antes citado."

   Y que originalmente implemente en Python 3, ya que no conozco muy bien C++.

   Pero aparte de algunas cuestiones esteticas, la implementacion es similar.
   Tambien, este programa no necesitaba esta complejidad, pero, tambien me sirve de practica
   Y tambien estaba bastante inspirado al escribirlo, tanto que agregue un extra, usar este descuento para calcular el costo
   de un medicamento.
   Los comentarios son para recordar que es lo que hice originalmente
   en algunos casos para ver que la implementacion original tambien era una forma valida, pero menos practica.

   En caso de querer reimplementar las versiones anteriores, se deberia usar / * texto * / para comentar la version
   actual y eliminar los /* y * / de las versiones anteriores a querer reimplementar
   Estos comentarios y aquellos que comienzan con // son ignorados completamente por el compilador
*/

/* Los #include incluyen modulos que son parte de la libreria estandar (std) de C++
   para añadir funcionalidad extra que de otra forma tendria que ser implementada por nosotros mismos.
   Estos modulos son preprocesados por el compilador para comprender el significado de aquellas funciones que nosotros llamamos
   dentro de este programa.
   A algunos genios (es decir la Organizacion Internacional para la Estandarizacion, comunmente conocida como ISO) de C++ se les ocurrio incluir
   las funciones mas comunmente implementadas para tener mas tiempo de hacer las cosas mencionadas anteriormente.

   El tambien puede contener archivos locales que esten en el mismo directorio que el codigo fuente que estas leyendo ahora.
   #include <stdlib.h> y #include <conio.h> incluyen archivos de cabecera que normalmente estan incluidos como un archivo cabecera.h
   en la carpeta donde el mismo codigo fuente (es decir, este mismo archivo) esta guardado.
   Estos contienen normalmente funciones que no son parte de la libreria estandar de C++, pero que algun alma caritativa se le ocurrio hacer
   para luego compartirla con el resto de nosotros mortales */

#include <iostream> /* cout (console output), cin (console input), y demas yerbas */
#include <locale>
#include <array> /* De aqui viene std::array */
#include <string> /* stof */
//#include <vector> /* Alternativa a array */
#include <algorithm> /* De aqui viene la funcion std::transform */
#include <stdlib.h> /* Modulo que no es parte de la libreria estandar, pero incluido por MinGW (y Visual Studio), que a su vez esta incluido en ZinjaI, contiene funciones especificas de Windows */
#include <conio.h> /* Modulo que no es parte de la libreria estandar, pero incluido por MinGW (y Visual Studio), que a su vez esta incluido en ZinjaI, contiene funciones especificas de la consola */




using namespace std; // Declarar que vamos a usar las funciones dentro de la libreria estandar


void capitalize(string &sentence)
{
  char sentence[10];

  sentence[0] = toupper(sentence[0]);

   for (int i = 1; i < size; i++)
   {
        if ( sentence[i - 1] == ' ' )
            sentence[i] = toupper( sentence[i] );
        else
            sentence[i] = tolower(sentence[i]);
   }

}

/* Mi entendimiento de las clases es limitado, pero esta clase esta para manejar el caso de el punto vs la coma
al tener que procesar numeros reales
   Esta clase reemplaza directamente parte del estandar para que al procesar puntos decimales en lugar quede una coma */
/* class WithComma: public numpunct<char>
{
	protected: char do_decimal_point() const { return ','; }
};*/

/* Funcion principal */

int main() {


  /* //v1: Declarar variables y constantes
  int plan_a = 100;
  int plan_boc = 90; // Mismo valor para el Plan B y el C
  int plan_d = 50;
  int plan_otro = 25;
  int desc_efec = 5; */

  // Variables
  // La inicializacion de la variable en C++ es simplemente declarar su tipo
  // Lo que no significa que no se le pueda asignar un valor, en cual caso se convierten en constantes
  // Tambien se pueden inicializar multiples variables al mismo tiempo.
  float descuento, fvalor_medicamento;

  /* locale myloc(locale(), new WithComma);*/



  // En C++ una vez que se define el tipo de la variable, esta se convierte en inmutable
  // La unica forma de utilizar la informacion que contiene en una variable de otro tipo
  // Es utilizando una variable mediadora, donde podamos convertir el valor de la funcion original a un valor compatible
  // Con el tipo de la variable con la que queremos hacer la operacion.
  string plan, efec, svalor_medicamento, desc_str; // std::string es parte de la libreria estandar
  /* stringstream *temp;* /

  /* v2: Reimplementar variables como listas */
  /* las listas o "arrays" estan definidas como array<tipo, numerodevalores> nombredelista {"valor_a", "valor_b", "valor_c", ..., "valor_z"}
     la utilidad de las listas es almacenar mucha informacion del mismo tipo bajo un solo nombre referenciable */
  array<int, 5> lista_desc{100, 90, 50, 25, 5};
  array<string, 5> lista_plan{"A", "B", "C", "D", "O"}; // En std:array se crea una lista de posibilidades
  array<string, 10> lista_resp{"si", "s", "SI", "Si", "S", "no", "No", "NO", "n", "N"};


  cout << "Ingrese el valor del medicamento: ";
  /* cin.imbue(myloc);
  cout.imbue(myloc); */
  cin >> fvalor_medicamento;

  /* Esta funcion es parte de stdlib.h, un modulo especifico que incluye funciones
     que referencian llamadas de sistema (es decir, que le pedimos al sistema operativo que haga algo en medio de la ejecucion)-
     Las funciones incluidas en stdlib.h normalmente son exclusivas al sistema operativo donde se compila este programa.


     En este caso, cls es una funcion exclusiva de Windows. Es decir, que no funcionara en otros sistemas operativos */

  system("cls");

  cout << "Calcule el descuento de un plan.\n\n";
  cout << "Planes validos: A, B, C, D.\nPara otros planes, ingrese O.\n";
  cout << "Ingrese el tipo de plan: ";
  cin >> plan;

  /* v3: Convertir entrada a MAYUSCULA, solo para los valores de plan

       std:transform es parte del modulo array
       El formato de transform es
       std::transform (variable_a_transformar.begin(), variable_a_transformar.end(), variable_a_transformar.begin(), metodoaemplear)
       El primer begin() marca que se deben contar los caracteres del valor de la variable desde su primera letra
       Es decir que si variable_a_transformar = "Pepe Botella", transform evalua desde la primera P de Pepe
       En end() especifica el caracter donde el valor de la varible termina
       El segundo begin() se utiliza para señalizar donde se tiene que guardar el cambio, es decir que la nueva variable se guarda en el mismo lugar donde
       esta aquella letra que estamos modificando.
       std::transform solo evalua un caracter a la vez, asi que hay que medir el largo del string ingresado por el usuario obligatoriamente.


       ::toupper es un metodo parte de la libreria estandar Y DE iostream, su funcion es convertir un caracter a su forma en mayuscula (siempre que tenga una)
       Al parecer la version en iostream no tiene exactamente la misma funcionalidad que la de la libreria estandar, asi que hay que referenciarla como ::toupper
       para que se interprete que forma parte del namespace std
       Lo cual es la razon por la que siempre especificamos "using namespace std" */

  std::transform (plan.begin(), plan.end(), plan.begin(), ::toupper);
  cout << "El plan ingresado es: " << plan << ".\n";


  /* Aqui se comprueba que el plan sea valido, es decir, que sea parte del array lista_plan.
     Sirve mas en programas mas largos, pero es para demostrar el concepto.
     std::find comprueba la lista y en caso de que plan no pertenezca a la lista, se retorna un valor vacio
     que es igual a lista_plan.end()
     nombredelista.end() hace referencia a un valor que esta despues del final de la lista referenciada.
     Es decir, un valor nulo dado que no esta incluido en la lista (duh)
     En este caso se cumple la comprobacion, y se vuelve al comienzo, hasta que la condicion _NO SEA CUMPLIDA_,
     Es decir, que el valor de plan pertenezca a listauento

     La comparacion entre dos variables es (variable1 == variable2), una comparacion retorna un valor booleano/logico, es decir, Verdadero o Falso.
     Mientras la condicion se cumpla, while va a continuar ejecutandose.
     */

  while (std::find(lista_plan.begin(), lista_plan.end(), plan) == lista_plan.end()) {
    system("cls");
    cout << "Este plan no es valido, por favor intente nuevamente.\n\n";
    cout << "Planes validos: A, B, C, D.\nPara otros planes, ingrese O.\n";
    cout << "Ingrese el tipo de plan: ";
    cin >> plan;
    cout << "El plan ingresado es: " << plan << ".\n";

}


/* v2:  Convertir entrada a minuscula, para simplificar los condicionales.

     std:transform es parte del modulo array
     El formato de transform es
     std::transform (variable_a_transformar.begin(), variable_a_transformar.end(), variable_a_transformar.begin(), metodoaemplear)
     El primer begin() marca que se deben contar los caracteres del valor de la variable desde su primera letra
     Es decir que si variable_a_transformar = "Pepe Botella", transform evalua desde la primera P de Pepe
     En end() especifica el caracter donde el valor de la varible termina
     El segundo begin() se utiliza para señalizar donde se tiene que guardar el cambio, es decir que la nueva variable se guarda en el mismo lugar donde
     esta aquella letra que estamos modificando.
     std::transform solo evalua un caracter a la vez, asi que hay que medir el largo del string ingresado por el usuario obligatoriamente.


     ::tolower es un metodo parte de la libreria estandar Y DE iostream, su funcion es convertir un caracter a su forma en miniscula (siempre que tenga una)
     Al parecer la version en iostream no tiene exactamente la misma funcionalidad que la de la libreria estandar, asi que hay que referenciarla como ::tolower
     para que se interprete que forma parte del namespace std
     Lo cual es la razon por la que siempre especificamos "using namespace std" */

 //  std::transform (plan.begin(), plan.end(), plan.begin(), ::tolower);



/* v1:
if (plan == "a" ) {
  descuento= plan_a;
} else if (plan == "b") {
  descuento= plan_boc;
} else if  (plan == "c") {
  descuento= plan_boc;
} else if (plan == "d") {
  descuento= plan_d;*/



/* v2 Comprobar plan, usando los valores guardados en el array listauento*/
/* Los valores de un array pueden ser referenciados por su posicion en la lista (comenzando por el 0, para el primer lugar).
   Los operadores logicos en C++ tambien incluyen ||, equivalente de "OR" (es decir, "O")
   Y &&, equivalente de "AND" (es decir "Y").

   Bajo el ejemplo de abajo, OR evalua si el valor de plan es igual al primer string de la lista listauento(es decir, "a"), o si su valor es igual
   al quinto string de la listauento(es decir, "A", y si, se distingue entre mayusculas y minisculas)
   En caso de que cualquier comparacion sea verdadera, se le asigna el valor de la primera posicion en lista_desc a
   la variable desc_plan. */

  if (plan == lista_plan[0] || plan == lista_plan[5]) {
    descuento = lista_desc[0];
  } else if ((plan == lista_plan[1] || plan == lista_plan[6]) || (plan == lista_plan[2] || plan == lista_plan[7])) {
    descuento = lista_desc[1];
  } else if  (plan == lista_plan[3] || plan == lista_plan[8]) {
    descuento = lista_desc[2];
  } else if (plan == lista_plan[4] || plan == lista_plan[9]) {
    descuento = lista_desc[3];
  }

  /* Preguntar si el usuario desea pagar en efectivo */
  /* Para simplificar los casos a evaluar, se excluye la pregunta al usuario del pago en efectivo
     En caso de que el valor de descuento sea el mismo de la primera posicion de lista_desc
     Es decir, en el caso de que el plan elegido sea el plan A, el cual tiene un 100% de descuento
     La razon de esto es logica, no puede existir un descuento mayor al 100%
     */
  if (descuento != lista_desc[0]) {
    cout << "Desea pagar en efectivo? Ingrese su respuesta: ";
    cin >> efec;

    while (std::find(lista_resp.begin(), lista_resp.end(), efec) == lista_resp.end()) {
      system("cls");
      cout << "Esta respuesta no es valida, por favor intente nuevamente.\n\n";
      cout << "Desea pagar en efectivo? Ingrese su respuesta: ";
      cin >> efec;
    }
  }
  system("cls");
  cout << "Su respuesta es: " << efec;
 // convertir los mayores de efec en minuscula
  std::transform (efec.begin(), efec.end(), efec.begin(), ::tolower);


  /* Procesar y calcular el descuento total -- mk1
  if (((efec == "si") || (efec == "s")) && (plan != "a")) {
  desc_total = descuento + desc_efec;
  } else {
  desc_total = desc_plan; */

  /* Procesar y calcular el descuento total -- mk2 */

  /* Para evaluar los valores posible de la variable efec
     Se utiliza un bucle for, que incrementa el valor de la variable i por cada ejecucion
     Cuando es definida, posee un valor inicial 0, y se ejecuta mientras i sea menor que 5
     A su vez, el valor de i referencia al valor de posicion de uno de los elementos del array lista_resp
     Con esto al mismo tiempo podemos evaluar multiples veces si el valor de efec es parte del array lista_resp.

     Notese como la variable "i" no fue especificada o inicializada al comienzo del programa.

     En caso de que el valor de efec se encuentre entre el valor de las 5 primeras posiciones de lista_resp
     Y que el valor de descuento NO SEA el de lista_desc[0] (es decir, 100%)
     Se le agrega el 5% del descuento extra por pagar en efectivo.
     */
  for (int i = 0; i < 5; i++) {
      if ((efec == lista_resp[i]) && (descuento != lista_desc[0])) {
        descuento = descuento + lista_desc[4];
      }
   }
  /* Calcular valor del medicamento */
  fvalor_medicamento = fvalor_medicamento * (1 - (descuento / 100));

  /* Mostrar datos finales */

  system("cls");
  cout << "Tipo de plan: " << plan << ".\n";

  /*#ifdef TEST_PARAM_1 // NO FUNCIONAL
  if ((efec_loc == "Si") && (descuento != lista_desc[0])) {
    cout << "Se desea pagar en efectivo: " << efec_loc << ".\n";
  } else if ((efec_loc == "No") && (descuento != lista_desc[0])) {
    cout << "Se desea pagar en efectivo: " << efec_loc << ".\n";
  }*/



  //#else
  /* Si descuento no es 100%, se pregunta sobre el pago en efectivo
     105% de descuento no es posible */
  if (descuento != lista_desc[0]) {
  // capitalize(efec);
  cout << "Se desea pagar en efectivo: " << efec << ".\n";
  }
  cout << "\nEl descuento total es de: " << descuento << "%.\n";
  //#endif




  cout << "\nEl valor del medicamento es: " << fvalor_medicamento << "\n";


  /* Pedir al usuario que presione una tecla para continuar: v1
     Solo Windows
  system("pause");
  */

  /* v2 Pedir al usuario que presione una tecla para continuar:
     Compatible con cualquier sistema operativo?            */

  cout << "\nPresione cualquier tecla para continuar...";

  /* Get Character -- getch(), parte de la libreria conio.h (Console I/O header)
     Normalmente getch() espera a que ingrese un caracter especificado, que deberia estar dentro del parentesis.
     Como no hay nada en el parentesis, getch esta esperando cualquier caracter.
     Como el valor que obtiene getch no es asignado a ninguna funcion ni existe otro condicional que necesite procesamiento, se continua la ejecucion. */
  getch();

  /* return devuelve un codigo de ejecucion como el resultado de la operacion main (que dijimos que era un int)
     Como parte de la interaccion del sistema operativo con el programa, los codigos de ejecucion le dicen al sistema operativo cual fue el resultado de la ejecucion
     de la funcion main.
     En este caso, return 0; le informa al sistema operativo que el programa se ejecuto correctamente */
  return 0;


}
