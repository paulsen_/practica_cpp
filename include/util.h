#include <iostream>
#include <algorithm> /* para la funcion std::replace */

#ifndef UTILSTUFF
#define UTILSTUFF

double convcpSTDP(std::string tempfunc) {
  /* Replace, parte del modulo std::algorithm, evalua la variable temp desde el principio hasta el final, y remplaza las comas por puntos */
  std::replace(tempfunc.begin(), tempfunc.end(), ',', '.');
  std::string::size_type sz;
  return stod(tempfunc, &sz);
}

std::string convcpDPST(double tempfunc) {
  std::string temp1 = std::to_string(tempfunc);
  std::replace(temp1.begin(), temp1.end(), '.', ',');
  return temp1;

}
#endif
