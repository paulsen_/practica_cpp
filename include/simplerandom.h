#include <random> /* para las funciones srand y rand() */
#include <ctime> /* para la funcion time proveniente del lenguaje C */
#include <cstring> /* para la funcion strcmp proveniente del lenguaje C */
#ifndef SIMPLERAND
#define SIMPLERAND

/* Generacion de numeros aleatorios */

void startRAND()
{
static bool first = true;
if (first)
  {
  srand(time(NULL));
  first = false;
  }
}


int randomINT(int min, int max) //rango : [minimo, maximo)
{
	startRAND();
	return (min + rand() % (( max + 1 ) - min));
}


double randomDP(double min, double max)
{
   startRAND();
   return ((max - min) * ((double)rand() / (double)RAND_MAX ) + min);
}


float randomFLOAT(float min, float max)
{
   startRAND();
   return ((max - min) * ((float)rand() / (float)RAND_MAX ) + min);
}
#endif
