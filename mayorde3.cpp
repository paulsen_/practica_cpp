#include <iostream>
using namespace std;

int main(int argc, char const *argv[]) {
  int n1, n2, n3;
  int menor, mayor;

  cout << "Ingrese el primer numero: \n";
  cin >> n1;
  cout << "Ingrese el segundo numero: \n";
  cin >> n2;
  cout << "Ingrese el tercer numero: \n";
  cin >> n3;

  cout << "DEBUG P0\n";
  cout << "DEBUG: valor n1: " << n1 << "\n";
  cout << "DEBUG: valor n2: " << n2 << "\n";
  cout << "DEBUG: valor n3: " << n3 << "\n";

  if ((n1 > n2) && (n1 > n3)) {
    mayor = n1;
    cout << "DEBUG P1: mayor: " << mayor << "\n";
  } else if ((n1 < n2) && (n1 < n3)) {
    menor = n1;
    cout << "DEBUG P1: menor: " << menor << "\n";
  }
  if ((n2 > n1) && (n2 > n3)) {
    mayor = n2;
    cout << "DEBUG P2: mayor: " << mayor << "\n";
  } else if ((n2 < n1) && (n2 < n3)) {
    menor = n2;
    cout << "DEBUG P2: menor: " << menor << "\n";
  }
  if ((n3 > n1) && (n3 > n2)) {
    mayor = n3;
    cout << "DEBUG P3: mayor: " << mayor << "\n";
  } else if ((n3 < n1) && (n3 < n2)) {
    menor = n3;
    cout << "DEBUG P3: menor: " << menor << "\n";
  }
  cout << "Numero mayor: " << mayor;
  return 0;
}
